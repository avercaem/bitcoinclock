long int VcolorNb = 0x000001;
long int *colorNb = &VcolorNb;
long int VcolorBack = 0x000000;
long int *colorBack = &VcolorBack;

long int *chiffres[10][7][4]={
  {{colorBack,colorNb,colorNb,colorBack},
  {colorNb,colorBack,colorBack,colorNb},
  {colorNb,colorBack,colorBack,colorNb},
  {colorNb,colorBack,colorBack,colorNb},
  {colorNb,colorBack,colorBack,colorNb},
  {colorNb,colorBack,colorBack,colorNb},
  {colorBack,colorNb,colorNb,colorBack}},


  {{colorBack,colorNb,colorBack,colorBack},
  {colorNb,colorNb,colorBack,colorBack},
  {colorBack,colorNb,colorBack,colorBack},
  {colorBack,colorNb,colorBack,colorBack},
  {colorBack,colorNb,colorBack,colorBack},
  {colorBack,colorNb,colorBack,colorBack},
  {colorNb,colorNb,colorNb,colorBack}},

  {{colorBack,colorNb,colorNb,colorBack},
  {colorNb,colorBack,colorBack,colorNb},
  {colorBack,colorBack,colorBack,colorNb},
  {colorBack,colorBack,colorNb,colorBack},
  {colorBack,colorNb,colorBack,colorBack},
  {colorNb,colorBack,colorBack,colorBack},
  {colorNb,colorNb,colorNb,colorNb}},

  {{colorBack,colorNb,colorNb,colorBack},
  {colorNb,colorBack,colorBack,colorNb},
  {colorBack,colorBack,colorBack,colorNb},
  {colorBack,colorNb,colorNb,colorBack},
  {colorBack,colorBack,colorBack,colorNb},
  {colorNb,colorBack,colorBack,colorNb},
  {colorBack,colorNb,colorNb,colorBack}},

  {{colorNb,colorBack,colorBack,colorBack},
  {colorNb,colorBack,colorBack,colorNb},
  {colorNb,colorBack,colorBack,colorNb},
  {colorNb,colorNb,colorNb,colorNb},
  {colorBack,colorBack,colorBack,colorNb},
  {colorBack,colorBack,colorBack,colorNb},
  {colorBack,colorBack,colorBack,colorNb}},

  {{colorNb,colorNb,colorNb,colorNb},
  {colorNb,colorBack,colorBack,colorBack},
  {colorNb,colorBack,colorBack,colorBack},
  {colorBack,colorNb,colorNb,colorBack},
  {colorBack,colorBack,colorBack,colorNb},
  {colorBack,colorBack,colorBack,colorNb},
  {colorNb,colorNb,colorNb,colorBack}},

  {{colorBack,colorNb,colorNb,colorBack},
  {colorNb,colorBack,colorBack,colorBack},
  {colorNb,colorBack,colorBack,colorBack},
  {colorNb,colorNb,colorNb,colorBack},
  {colorNb,colorBack,colorBack,colorNb},
  {colorNb,colorBack,colorBack,colorNb},
  {colorBack,colorNb,colorNb,colorBack}},

  {{colorNb,colorNb,colorNb,colorBack},
  {colorBack,colorBack,colorNb,colorBack},
  {colorBack,colorBack,colorNb,colorBack},
  {colorBack,colorNb,colorNb,colorBack},
  {colorBack,colorBack,colorNb,colorBack},
  {colorBack,colorBack,colorNb,colorBack},
  {colorBack,colorBack,colorNb,colorBack}},

  {{colorBack,colorNb,colorNb,colorBack},
  {colorNb,colorBack,colorBack,colorNb},
  {colorNb,colorBack,colorBack,colorNb},
  {colorBack,colorNb,colorNb,colorBack},
  {colorNb,colorBack,colorBack,colorNb},
  {colorNb,colorBack,colorBack,colorNb},
  {colorBack,colorNb,colorNb,colorBack}},

  {{colorBack,colorNb,colorNb,colorBack},
  {colorNb,colorBack,colorBack,colorNb},
  {colorNb,colorBack,colorBack,colorNb},
  {colorBack,colorNb,colorNb,colorNb},
  {colorBack,colorBack,colorBack,colorNb},
  {colorBack,colorBack,colorBack,colorNb},
  {colorBack,colorNb,colorNb,colorBack}},
};
