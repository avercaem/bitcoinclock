#include <Adafruit_NeoPixel.h>
#include "variables.h"

#define PIN D2
#define NBPIXELS 320

Adafruit_NeoPixel pixels(NBPIXELS, PIN, NEO_GRB + NEO_KHZ800);

int sc=0;

void intToDigits(long int nb,int digits[] ){
  for (int i =0;i<8;i++){
    digits[i]=nb % 10;
    nb /=10;
  }
}

int intLenght(long int nb){
  return floor(log10(abs(nb))) + 1;
}

void afficherChiffre(long int nb,int Cstart, int lenght){
  int digits[7]; 
  int idP;

  if (lenght<=0){
    lenght=intLenght(nb);
  }
  
  if (Cstart >=0){
    idP=Cstart*8;
  }
  else{
    if (lenght%2==0)idP = 160 - (lenght*2.5)*8;
    else{
      idP = 164 - (lenght*2.5)*8;
    }
  }
  
  intToDigits(nb,digits);
  for (int i=0;i<lenght;i++){
    for(int k =3;k>=0; k--){
      idP++;
      for (int j = 0;j<7;j++){
        pixels.setPixelColor(idP++,*chiffres[digits[i]][j][k]);
      }
    }
    idP+=8;
  }
}

void afficher2dots(int colomn){
  pixels.setPixelColor(colomn*8+3, *colorNb);
  pixels.setPixelColor(colomn*8+5, *colorNb);
}

void afficherHeure(int heure, int minute,int seconde){
  afficherChiffre(heure,27,2);
  afficher2dots(25);
  afficherChiffre(minute,15,2);
  afficher2dots(13);
  afficherChiffre(seconde,3,2);
}

void setup() {
  pixels.begin();
  pixels.clear();
}

void loop() {
  *colorNb=0x0A0A02;
  afficherHeure(14,55,sc);
  pixels.show();
  pixels.clear();
  delay(1950);
  afficherChiffre(2020,-1,0);
  pixels.show();
  pixels.clear();
  sc+=5;
  delay(2950);
}
