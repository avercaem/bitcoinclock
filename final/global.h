#ifndef STASSID
#define STASSID "Livebox-C3E0"
#define STAPSK  "3X4NcLGcemjvSmQrs3"
#endif

#define PIN D2
#define NBPIXELS 320

Adafruit_NeoPixel pixels(NBPIXELS, PIN, NEO_GRB + NEO_KHZ800);

const char* ssid = STASSID;
const char* password = STAPSK;

const char* host = "cex.io";
const int httpsPort = 443;

int minutes=0;
int heures=0;

int ETHPrice = 0;
int ETHLow =0;
int ETHHigh = 0;
int oldETHValue=0;
long int oldETHColor = 0x0f0f0f;
float ETHVariabilite = 0;

int BTCPrice = 0;
int BTCLow = 0;
int BTCHigh = 0;
int oldBTCValue=0;
long int oldBTCColor = 0x0f0f0f;
float BTCVariabilite = 0;
