long int VcolorNb = 0x0f0f0a;
long int *colorNb = &VcolorNb;
long int VcolorBack = 0x000000;
long int *colorBack = &VcolorBack;

long int *chiffres[10][7][4]={
  {{colorBack,colorNb,colorNb,colorBack},
  {colorNb,colorBack,colorBack,colorNb},
  {colorNb,colorBack,colorBack,colorNb},
  {colorNb,colorBack,colorBack,colorNb},
  {colorNb,colorBack,colorBack,colorNb},
  {colorNb,colorBack,colorBack,colorNb},
  {colorBack,colorNb,colorNb,colorBack}},


  {{colorBack,colorNb,colorBack,colorBack},
  {colorNb,colorNb,colorBack,colorBack},
  {colorBack,colorNb,colorBack,colorBack},
  {colorBack,colorNb,colorBack,colorBack},
  {colorBack,colorNb,colorBack,colorBack},
  {colorBack,colorNb,colorBack,colorBack},
  {colorNb,colorNb,colorNb,colorBack}},

  {{colorBack,colorNb,colorNb,colorBack},
  {colorNb,colorBack,colorBack,colorNb},
  {colorBack,colorBack,colorBack,colorNb},
  {colorBack,colorBack,colorNb,colorBack},
  {colorBack,colorNb,colorBack,colorBack},
  {colorNb,colorBack,colorBack,colorBack},
  {colorNb,colorNb,colorNb,colorNb}},

  {{colorBack,colorNb,colorNb,colorBack},
  {colorNb,colorBack,colorBack,colorNb},
  {colorBack,colorBack,colorBack,colorNb},
  {colorBack,colorNb,colorNb,colorBack},
  {colorBack,colorBack,colorBack,colorNb},
  {colorNb,colorBack,colorBack,colorNb},
  {colorBack,colorNb,colorNb,colorBack}},

  {{colorNb,colorBack,colorBack,colorBack},
  {colorNb,colorBack,colorBack,colorNb},
  {colorNb,colorBack,colorBack,colorNb},
  {colorNb,colorNb,colorNb,colorNb},
  {colorBack,colorBack,colorBack,colorNb},
  {colorBack,colorBack,colorBack,colorNb},
  {colorBack,colorBack,colorBack,colorNb}},

  {{colorNb,colorNb,colorNb,colorNb},
  {colorNb,colorBack,colorBack,colorBack},
  {colorNb,colorBack,colorBack,colorBack},
  {colorBack,colorNb,colorNb,colorBack},
  {colorBack,colorBack,colorBack,colorNb},
  {colorBack,colorBack,colorBack,colorNb},
  {colorNb,colorNb,colorNb,colorBack}},

  {{colorBack,colorNb,colorNb,colorBack},
  {colorNb,colorBack,colorBack,colorBack},
  {colorNb,colorBack,colorBack,colorBack},
  {colorNb,colorNb,colorNb,colorBack},
  {colorNb,colorBack,colorBack,colorNb},
  {colorNb,colorBack,colorBack,colorNb},
  {colorBack,colorNb,colorNb,colorBack}},

  {{colorNb,colorNb,colorNb,colorBack},
  {colorBack,colorBack,colorNb,colorBack},
  {colorBack,colorBack,colorNb,colorBack},
  {colorBack,colorNb,colorNb,colorNb},
  {colorBack,colorBack,colorNb,colorBack},
  {colorBack,colorBack,colorNb,colorBack},
  {colorBack,colorBack,colorNb,colorBack}},

  {{colorBack,colorNb,colorNb,colorBack},
  {colorNb,colorBack,colorBack,colorNb},
  {colorNb,colorBack,colorBack,colorNb},
  {colorBack,colorNb,colorNb,colorBack},
  {colorNb,colorBack,colorBack,colorNb},
  {colorNb,colorBack,colorBack,colorNb},
  {colorBack,colorNb,colorNb,colorBack}},

  {{colorBack,colorNb,colorNb,colorBack},
  {colorNb,colorBack,colorBack,colorNb},
  {colorNb,colorBack,colorBack,colorNb},
  {colorBack,colorNb,colorNb,colorNb},
  {colorBack,colorBack,colorBack,colorNb},
  {colorBack,colorBack,colorBack,colorNb},
  {colorBack,colorNb,colorNb,colorBack}},
};

long int Vblue1 = 0x0c0c3f;
long int *blue1 = &Vblue1;

long int Vblue2 = 0x0a0a1f;
long int *blue2 = &Vblue2;

long int Vblue3 = 0x04040d;
long int *blue3 = &Vblue3;

long int Vblue4 = 0x000005;
long int *blue4 = &Vblue4;

long int *ETHLogo[8][8]{
  {colorBack,colorBack,colorBack,blue1,blue2,colorBack,colorBack,colorBack},
  {colorBack,colorBack,blue1,blue1,blue2,blue2,colorBack,colorBack},
  {colorBack,blue1,blue3,blue3,blue4,blue4,blue2,colorBack},
  {colorBack,blue3,blue3,blue3,blue4,blue4,blue4,colorBack},
  {colorBack,colorBack,blue3,blue3,blue4,blue4,colorBack,colorBack},
  {colorBack,blue1,colorBack,colorBack,colorBack,colorBack,blue2,colorBack},
  {colorBack,colorBack,blue1,blue1,blue2,blue2,colorBack,colorBack},
  {colorBack,colorBack,colorBack,blue1,blue2,colorBack,colorBack,colorBack}
};

long int Vwhite = 0x0f0f0a;
long int *white = &Vwhite;

long int Vyellow = 0x040200;
long int *yellow = &Vyellow;

long int *BTCLogo[8][8]{
  {colorBack,yellow,white,yellow,white,yellow,yellow,colorBack},
  {yellow,white,white,white,white,white,yellow,yellow},
  {yellow,yellow,white,yellow,white,yellow,white,yellow},
  {yellow,yellow,white,yellow,white,yellow,white,yellow},
  {yellow,yellow,white,white,white,white,yellow,yellow},
  {yellow,yellow,white,yellow,white,yellow,white,yellow},
  {yellow,white,white,white,white,white,yellow,yellow},
  {colorBack,yellow,white,yellow,white,yellow,yellow,colorBack},
};
