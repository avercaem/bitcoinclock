#include <Adafruit_NeoPixel.h>
#include <ESP8266WiFi.h>
#include <WiFiClientSecure.h>

#include "affichage.h"
#include "global.h"

void intToDigits(long int nb,int digits[] ){
  int lenght=intLenght(nb);
  for (int i =0;i<lenght;i++){
    digits[i]=nb % 10;
    Serial.print(i);
    Serial.print(" : ");
    Serial.println(digits[i]);
    nb /=10;
  }
}

int intLenght(long int nb){
  return floor(log10(abs(nb))) + 1;
}

void afficherChiffre(long int nb,int Cstart, int lenght){
  int digits[7]; 
  int idP;

  if (lenght<=0){
    lenght=intLenght(nb);
  }
  
  if (Cstart >=0){
    idP=Cstart*8;
  }
  else{
    if (lenght%2==0)idP = 160 - (lenght*2.5)*8;
    else{
      idP = 164 - (lenght*2.5)*8;
    }
    idP+=(Cstart+1)*8;
  }
  
  intToDigits(nb,digits);
  for (int i=0;i<lenght;i++){
    for(int k =3;k>=0; k--){
      idP++;
      for (int j = 0;j<7;j++){
        pixels.setPixelColor(idP++,*chiffres[digits[i]][j][k]);
      }
    }
    idP+=8;
  }
}

void afficher2dots(int colomn){
  pixels.setPixelColor(colomn*8+3, *colorNb);
  pixels.setPixelColor(colomn*8+5, *colorNb);
}

void afficherHeure(){
  int Trueheures= heures+2;
  if(Trueheures>=24){
    Trueheures-=24;
  }
  afficherChiffre(Trueheures,23,2);
  afficher2dots(20);
  afficherChiffre(minutes,9,2);
}

void getInfo(String arg1, String arg2){
  WiFiClientSecure client;
  client.setInsecure();
  Serial.print("connecting to ");
  Serial.println(host);

  if (!client.connect(host, httpsPort)) {
    Serial.println("connection failed");
    return;
  }

  String url = "/api/ticker/"+arg1+"/"+arg2;
  Serial.print("requesting URL: ");
  Serial.println(url);

  client.print(String("GET ") + url + " HTTP/1.1\r\n" +
               "Host: " + host + "\r\n" +
               "User-Agent: BuildFailureDetectorESP8266\r\n" +
               "Connection: close\r\n\r\n");

  Serial.println("request sent");

  String Sprice = client.readStringUntil(':');
  String Slow;
  String Shigh;
  String Svariabilite;
  String date = client.readStringUntil('\n');
  Serial.println(date);
  heures= date.substring(18,20).toInt();
  Serial.println(heures);
  minutes= date.substring(21,23).toInt();
  Serial.println(minutes);
  client.readStringUntil('{');
  for (int i =0;i<7;i++){
    client.readStringUntil('"');
  }
  Slow = client.readStringUntil('"');
  for (int i =0;i<3;i++){
    client.readStringUntil('"');
  }
  Shigh =client.readStringUntil('"');
  for (int i =0;i<3;i++){
    client.readStringUntil('"');
  }
  Sprice = client.readStringUntil('"');
  for (int i =0;i<19;i++){
    client.readStringUntil('"');
  }
  Svariabilite = client.readStringUntil('"');
  if (arg1=="ETH"){
    ETHPrice = Sprice.toInt();
    ETHLow = Slow.toInt();
    ETHHigh = Shigh.toInt();
    ETHVariabilite = Svariabilite.toFloat();
  }
  else{
    BTCPrice = Sprice.toInt();
    BTCLow = Slow.toInt();
    BTCHigh = Shigh.toInt();
    BTCVariabilite = Svariabilite.toFloat();
  }
}

void setColor(String crypto){
  if (crypto=="ETH"){
    if (oldETHValue<ETHPrice){
      *colorNb = 0x000F00;
      oldETHValue=ETHPrice;
      oldETHColor=*colorNb;
    }
    else if (oldETHValue>ETHPrice){
      *colorNb = 0x0F0000;
      oldETHValue=ETHPrice;
      oldETHColor=*colorNb;
    }
    else{
      *colorNb = oldETHColor;
    }
  }
  else{
    if (oldBTCValue<BTCPrice){
      *colorNb = 0x000F00;
      oldBTCValue=ETHPrice;
      oldBTCColor=*colorNb;
    }
    else if (oldBTCValue>BTCPrice){
      *colorNb = 0x0f0000;
      oldBTCValue=BTCPrice;
      oldBTCColor=*colorNb;
    }
    else{
      *colorNb = oldBTCColor;
    }
  }
}

void afficherVariabilite(String crypto){
  int index;
  long int tmpColor = *colorNb;
  int green = 31; 
  int red = 0;
  float variabilite = 0;
  if(crypto == "ETH"){
    index = map(ETHPrice, ETHLow, ETHHigh, 30,1);
    variabilite = ETHVariabilite;
  }
  else{
    index = map(BTCPrice, BTCLow, BTCHigh, 30,1);
    variabilite = BTCVariabilite;
  }
  *colorNb = pixels.Color(red, green, 0);
  for(int i=0;i<32;i++){
    if(i>index){
      *colorNb = pixels.Color(red, green, 0);
      pixels.setPixelColor(i*8, *colorNb);
    }
    red++;
    green--;
  }
  if (variabilite >=0){
    *colorNb = 0x001e00;
    index = map(variabilite, 0, 8, 7,0);
    for(int i = 7;i>=index;i--){
      pixels.setPixelColor(i, *colorNb);
    }
  }
  else{
    *colorNb = 0x1e0000;
    index = map(variabilite, 0, -8, 0,7);
    Serial.print("var : ");
    Serial.println(index);
    for(int i = 0;i<=index;i++){
      pixels.setPixelColor(i, *colorNb);
    }
  }
  *colorNb = tmpColor;
}

void afficherLogo(String crypto){
  int idP=256;
  int color = 100;
  for (int i=7;i>=0;i--){
    for (int j=0;j<8;j++){
      if (crypto == "ETH"){
        pixels.setPixelColor(idP++, *ETHLogo[j][i]);
      }
      else{
        pixels.setPixelColor(idP++, *BTCLogo[j][i]);
      }
    }
  }
}

void setup() {
  pixels.begin();
  pixels.clear();

  Serial.begin(115200);
  Serial.println();
  Serial.print("connecting to ");
  Serial.println(ssid);
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
}

void loop() {
  *colorNb=0x0f0f0a;
  getInfo("BTC","EUR");
  getInfo("ETH","EUR");
  for(int i =0;i<4;i++){
    *colorNb = 0x0f0f0a;
    pixels.clear();
    afficherHeure();
    pixels.show();
    delay(5000); 
    pixels.clear(); 
    setColor("BTC");
    afficherLogo("BTC");
    afficherChiffre(BTCPrice,-5,0);
    afficherVariabilite("BTC");
    pixels.show();
    delay(5000);
    pixels.clear();  
    setColor("ETH");
    afficherLogo("ETH");
    afficherChiffre(ETHPrice,-5,0);
    afficherVariabilite("ETH");
    pixels.show();
    delay(5000);
  }
}
